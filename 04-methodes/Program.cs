﻿using System;

namespace _04_methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            double m = Multiplier(3.6, 4.5);
            Afficher(m);
            // Appel de methode (sans retour)
            Afficher(12.345);

            // Exercice Maximum
            int v1 = Convert.ToInt32(Console.ReadLine());
            int v2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Maximum(v1, v2));

            // Exercice Paire
            Console.WriteLine(Paire(4));
            Console.WriteLine(Paire(5));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int a = 12;
            TestParamValeur(a);
            Console.WriteLine(a);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            int b = 10;
            TestParamReference(ref b);
            Console.WriteLine(b);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée
            int o1, o2;
            TestParamSortie(out o1, out o2);
            Console.WriteLine($"o1={o1} o2={o2}");

            // Déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            TestParamSortie(out int o3, out int o4);
            Console.WriteLine($"{o3},{o4}");

            // On peut ignorer un paramètre out en le nommant _
            TestParamSortie(out _, out int o5);
            Console.WriteLine(o5);


            // Paramètre optionnel
            TestParamOptionel(false, 1.2, 10);
            TestParamOptionel(true, 8.9);
            TestParamOptionel(true);

            // Paramètres nommés
            TestParamOptionel(i: 4, b: false, d: 9.8);
            TestParamOptionel(b: true, i: 23);

            // Nombre de paramètres variable
            double moy = Moyenne(3, 4);
            Console.WriteLine(moy);
            Console.WriteLine(Moyenne(4, 5, 7, 8, 5, 5));
            Console.WriteLine(Moyenne());

            // Exercice Tableau
            int[] t = { 3, 6, 1, 8, -4 };
            AfficherTab(t);
            CalculTab(t, out int max, out double moyenne);
            Console.WriteLine($"maximum={max} moyenne={moy}");

            //Exercice Echange
            int e1 = 3;
            int e2 = 4;
            Console.WriteLine($"e1={e1} e2={e2}");
            Echange(ref e1, ref e2);
            Console.WriteLine($"e1={e1} e2={e2}");

            // Surcharge de méthode
            Console.WriteLine(Somme(1, 2));                   // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1.5, 2.6));
            Console.WriteLine(Somme("Hello ", "world"));
            Console.WriteLine(Somme(1, 2, 3));
            Console.WriteLine(Somme(123L, 456L));           // pas de correspondance exacte => convertion automatique de long en double => appel de la méthode avec 2 double en paramètres
                                                            //  Console.WriteLine(Somme(123.5M, 456.7M));   // erreur pas de conversion possible

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (var ar in args)
            {
                Console.WriteLine(ar);
            }

            // Méthode récursive
            int fact3 = Factorial(3);
            Console.WriteLine(fact3);
            Console.ReadKey();
        }

        static double Multiplier(double d1, double d2)
        {
            return d1 * d2;     //  L'instruction return
                                // - Interrompt l'exécution de la méthode
                                // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(double d)   // void => pas de valeur retourné
        {
            Console.WriteLine($"valeur={d}");
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int a, int b)
        {
            if (a > b)
            {
                return a;
            }
            else
            {
                return b;
            }
            // return a > b ? a : b;
        }
        #endregion

        #region Exercice Paire
        // Écrire une méthode Paire qui prend un entier en paramètre un entier
        // Elle retourne vrai, si il est paire
        static bool Paire(int valeur)
        {
            //if(valeur %2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return valeur % 2 == 0;
        }
        #endregion

        #region Passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int x)
        {
            Console.WriteLine(x);
            x = 100;     // La modification de la valeur du paramètre x n'a pas d'influence en dehors de la méthode
            Console.WriteLine(x);
        }

        // Passage par référence => ref
        static void TestParamReference(ref int x)
        {
            Console.WriteLine(x);
            x = 100;
            Console.WriteLine(x);
        }

        // Passage de paramètre en sortie => out
        static void TestParamSortie(out int x, out int y)
        {
            // Console.WriteLine(x); // erreur 
            x = 100;    // La méthode doit obligatoirement affecter une valeur aux paramètres out
            y = 101;
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionel(bool b, double d = 6.7, int i = 4)
        {
            Console.WriteLine($"b={b} d={d} i={i}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(params int[] valeur)
        {
            double somme = 0;
            for (int i = 0; i < valeur.Length; i++)
            {
                somme += valeur[i];
            }
            if (valeur.Length > 0)
            {
                return somme / valeur.Length;
            }
            else
            {
                return 0.0;
            }
        }
        #endregion

        #region Exercice Echange
        static void Echange(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }
        #endregion

        #region Exercice Tableau
        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui calcule :
        // - le maximum d’un tableau d’entier
        // - la moyenne
        static void AfficherTab(int[] valeurs)
        {
            foreach (var v in valeurs)
            {
                Console.Write($"{v} ");
            }
        }

        static void CalculTab(int[] t, out int maximum, out double moyenne)
        {

            maximum = Int32.MinValue;
            double somme = 0.0;
            foreach (var elm in t)
            {
                if (elm > maximum)
                {
                    maximum = elm;
                }
                somme += elm;
            }
            moyenne = somme / t.Length;
        }
        #endregion

        #region Surcharge_de_méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature

        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }

        static double Somme(double v, double w)
        {
            Console.WriteLine("2 double");
            return v + w;
        }

        static double Somme(double a, int b)
        {
            Console.WriteLine("Un entier un double");
            return a + b;
        }

        static string Somme(string s1, string s2)
        {
            Console.WriteLine("Deux chaine de caractères");
            return s1 + s2;
        }

        static int Somme(int x, int y, int z)
        {
            Console.WriteLine("3 entiers");
            return x + y + z;
        }
        #endregion

        #region Méthode récursive
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion
    }
}
