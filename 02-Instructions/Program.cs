﻿using System;

namespace _02_Instructions
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Condition
            // Condition if
            /*            int c = Convert.ToInt32(Console.ReadLine());
                        if (c > 10)
                        {
                            Console.WriteLine("c est supérieur à 10");
                        }
                        else if (c == 10)
                        {
                            Console.WriteLine("c est égal à 10");
                        }
                        else
                        {
                            Console.WriteLine("c est inférieur à 10");
                        }

                        // Exercice: Trie de 2 Valeurs
                        // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
                        int va = Convert.ToInt32(Console.ReadLine());
                        int vb = Convert.ToInt32(Console.ReadLine());
                        if (va > vb)
                        {
                            Console.WriteLine($"{vb} < {va}");
                        }
                        else
                        {
                            Console.WriteLine($"{va} < {vb}");
                        }

                        // Exercice: Intervalle
                        // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
                        Console.WriteLine("Saisir un nombre entier");
                        int vi = Convert.ToInt32(Console.ReadLine());
                        if (vi > -4 && vi <= 7)
                        {
                            Console.WriteLine($"{vi} fait parti de l'intervalle -4,7");
                        }

                        // Condition switch
                        int jours = Convert.ToInt32(Console.ReadLine());
                        switch (jours)
                        {
                            case 1:
                                Console.WriteLine("Lundi");
                                break;
                            case 6:
                            case 7:
                                Console.WriteLine("Week end !");
                                break;
                            default:
                                Console.WriteLine("Un autre jour");
                                break;
                        }

                        // Exercice: Calculatrice
                        // Faire un programme calculatrice
                        // Saisir dans la console
                        // - un nombre à virgule flottante v1
                        // - une chaîne de caractère opérateur qui a pour valeur valide: __ + -* / __
                        // - un nombre à virgule flottante v2
                        // Afficher:
                        // - Le résultat de l’opération
                        // - Un message d’erreur si l’opérateur est incorrect
                        // - Un message d’erreur si l’on fait une division par 0
                        double v1 = Convert.ToDouble(Console.ReadLine());
                        string op = Console.ReadLine();
                        double v2 = Convert.ToDouble(Console.ReadLine());
                        switch (op)
                        {
                            case "+":
                                Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                                break;
                            case "-":
                                Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                                break;
                            case "*":
                                Console.WriteLine($"{v1} x {v2} = {v1 * v2}");
                                break;
                            case "/":
                                if (v2 == 0.0)
                                {
                                    Console.WriteLine("Division par 0");
                                }
                                else
                                {
                                    Console.WriteLine($"{v1} / {v2} = {v1 / v2}");
                                }
                                break;
                            default:
                                Console.WriteLine($"L'opérateur {op} est incorrect");
                                break;
                        }

                        // Clause when (C# 7.0) => Ajouter une condition supplémentaire sur un case
                        jours = Convert.ToInt32(Console.ReadLine());
                        switch (jours)
                        {
                            case 1:
                                Console.WriteLine("Lundi");
                                break;
                            case int x when x > 5 && x < 8:
                                Console.WriteLine("Week end !");
                                break;
                            default:
                                Console.WriteLine("Un autre jour");
                                break;
                        }

                        // Opérateur ternaire
                        double ta = Convert.ToDouble(Console.ReadLine());
                        double tb = Convert.ToDouble(Console.ReadLine());
                        double maximum = ta < tb ? tb : ta;
                        Console.WriteLine($"Maximum={maximum}");
                        #endregion


                        #region Condition
                        // while
                        int k = 0;
                        while (k < 10)
                        {
                            Console.WriteLine($"k= {k}");
                            k++;
                        }

                        // do while
                        k = 0;
                        do
                        {
                            Console.WriteLine($"k= {k}");
                            k++;
                        } while (k < 10);

                        // for
                        for (int i = 0; i < 10; i++)
                        {
                            Console.WriteLine($"i= {i}");
                        }

                        //for(; ; ) // Boucle infinie
                        //{

                        //}


                        // Instruction de saut

                        // break
                        for (int i = 0; i < 10; i++)
                        {
                            Console.WriteLine($"i= {i}");
                            if (i == 3)
                            {
                                break; // break => terminer la boucle 
                            }
                        }

                        // continue
                        for (int i = 0; i < 10; i++)
                        {
                            if (i == 3)
                            {
                                continue; // continue => on passe un itération suivante
                            }
                            Console.WriteLine($"i= {i}");
                        }

                        // goto
                        for (int i = 0; i < 10; i++)
                        {
                            for(int j = 0; j < 3; j++)
                            {
                                Console.WriteLine($"i= {i} j={j}");
                                if(i==2)
                                {
                                    goto EXIT_LOOP;
                                }
                            }
                        }
                EXIT_LOOP:     
                      for(; ; ) // while(true)
                        {
                            int m=Convert.ToInt32(Console.ReadLine());
                            if (m<1 || m > 9)
                            {
                                break;
                            }
                            for(int i=1;i<10;i++)
                            {
                                Console.WriteLine($"{i} x {m} = {i * m}");
                            }
                        }

                        Console.Write("Saisir le nombre de colonne=");
                        int col = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Saisir le nombre de ligne=");
                        int row = Convert.ToInt32(Console.ReadLine());
                        for (int r = 0; r < row; r++)
                        {
                            for (int c = 0; c < col; c++)
                            {
                                Console.Write("[ ] ");
                            }
                            Console.WriteLine("");
                        }
                        */

            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto default; // goto case 6;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            #endregion
            Console.ReadKey();
        }
    }
}
