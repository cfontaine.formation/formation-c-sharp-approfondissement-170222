﻿using System;
using System.Text;

namespace _01_Base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Motorisation { ESSENCE = 95, DIESEL = 45, GPL = 4, ELECTRIQUE = 220, BIODIESEL = 90 }
    enum Direction : short { NORD = 90, OUEST = 180, SUD = 270, EST = 0 }

    [Flags]
    enum Jour
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    }
    class Program
    {
        static void Main(string[] args)
        {
            #region Variable et types simple
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable
            double hauteur, largeur;
            hauteur = 2.4;
            largeur = 45.6;
            Console.WriteLine(hauteur + " " + largeur); // + => concaténation

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            int j = 12;
            int j1 = 1, j2 = 45;
            Console.WriteLine(j + " " + j1 + " " + j2);

            // Littétral booléen
            bool test = false; // true
            Console.WriteLine(test);

            // Littéral caractère
            char chr = 'a';
            char chrUtf8 = '\u0045';     // Caractère en UTF-8
            char chrUtf8Hexa = '\x45';
            Console.WriteLine(chr + " " + chrUtf8 + " " + chrUtf8Hexa);

            // Littéral entier -> int par défaut
            long l = 12L; // L-> long 
            uint ui = 12U; // U -> unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> chagement de base
            int dec = 10;           // décimal (base 10) par défaut
            int hexa = 0xFF2;       // 0x => héxadécimal (base 16)
            int bin = 0b100101010;  // 0b => binaire (base 2)
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante => par défaut double
            float f = 12.3F;        // F -> Littéral float
            decimal deci = 12.3M;   // M -> Littéral decimal
            Console.WriteLine(f + " " + deci);

            // Littéral nombre à virgule flottante
            double d3 = 100.4;
            double exp = 3.5e3;
            Console.WriteLine(d3 + " " + exp);

            // Séparateur _
            int sep = 1_000_000;
            double sep2 = 1_000.50_45;  // pas de séparateur en début, en fin , avant et après la virgule
            Console.WriteLine(sep + " " + sep2);

            // Type implicite -> var
            var v1 = 10.2;      // v1 -> double
            var v2 = "azerty";  // v2 -> string
            Console.WriteLine(v1 + " " + v2);

            // avec @ on peut utiliser les mots réservés comme nom de variable (à éviter)
            int @while = 12;
            Console.WriteLine(@while);
            #endregion

            #region Convertion
            // Transtypage implicite: ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 123;
            long til = tii;
            double tid = tii;
            Console.WriteLine(tii + " " + til + " " + tid);

            // Transtypage explicite: cast = (nouveauType)
            double ted = 2.3;
            int tei = (int)ted;
            decimal tedc = (decimal)ted;
            Console.WriteLine(ted + " " + tei + " " + tedc);

            // Dépassement de capacité
            int sh1 = 300;              // 00000000 00000000 00000001 00101100    300
            sbyte sb1 = (sbyte)sh1;     //                            00101100    44
            Console.WriteLine(sb1);

            // checked / unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    sb1 = (sbyte)sh1;
            //    Console.WriteLine(sb1);   // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            unchecked // (par défaut )
            {
                sb1 = (sbyte)sh1;
                Console.WriteLine(sb1);     // plus de vérification de dépassement de capacité
            }

            // Fonction de Convertion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = 42;
            double cnv1 = Convert.ToDouble(fc1);    // Convertion d'un entier en double

            string str = "123";
            int cnv2 = Convert.ToInt32(str);
            Console.WriteLine(cnv1 + " " + cnv2);   // Convertion d'une chaine de caractère  en entier

            Console.WriteLine(Convert.ToString(cnv2));
            // Conversion d'un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(cnv2, 2)); // binaire
            Console.WriteLine(Convert.ToString(cnv2, 16)); // hexadécimal

            // Conversion d'une chaine de caractères en entier
            // Parse
            int cnv3 = Int32.Parse("456");
            // cnv3= Int32.Parse("azerty");     //  Erreur => génère une exception
            Console.WriteLine(cnv3);

            // TryParse
            int cnv4;
            bool tstCnv = Int32.TryParse("123", out cnv4);  // Retourne true et la convertion est affecté à cnv4
            Console.WriteLine(cnv4 + " " + tstCnv);
            tstCnv = Int32.TryParse("azerty", out cnv4);    // Erreur => retourne false, 0 est affecté à cnv4
            Console.WriteLine(cnv4 + " " + tstCnv);
            #endregion


            #region Type référence
            StringBuilder s1 = new StringBuilder("hello");
            StringBuilder s2 = null;
            Console.WriteLine(s1 + " " + s2);

            s2 = s1;
            Console.WriteLine(s1 + " " + s2);

            s2 = null;
            s1 = null;  // str1 et str2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il éligible à la destruction par le garbage collector
            Console.WriteLine(s1 + " " + s2);
            #endregion

            #region type nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            int? tn = null;

            Console.WriteLine(tn.HasValue); // tn == null retourne false
            tn = 123;                       // Conversion implicite (int vers nullable)
            Console.WriteLine(tn.HasValue); // La propriété HasValue retourne true si  tn contient une valeur (!= null)

            Console.WriteLine(tn.Value); // Pour récupérer la valeur, on peut utiliser la propriété Value
            Console.WriteLine((int)tn);  // ou, on peut faire un cast
            #endregion

            #region Constante
            const double PI = 3.14;
            // PI = 2;      // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(2 * PI);
            #endregion

            #region Format de chaine de caractères
            int xi = 1;
            int yi = 3;
            string resFormat = string.Format("xi={0}  yi={1}", xi, yi);
            Console.WriteLine(resFormat);

            Console.WriteLine("xi={0}  yi={1}", xi, yi);    // on peut définir directement le format dans la mèthode WriteLine

            Console.WriteLine($"xi={xi}\nyi={yi}");

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            Console.WriteLine("c:\tmp\newfile.txt");
            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers) 
            Console.WriteLine(@"c:\tmp\newfile.txt");
            #endregion

            #region Saisir une valeur au clavier
            string line = Console.ReadLine();
            Console.WriteLine(line);
            #endregion;

            #region Opérateur
            // Operateur arithméthique
            double op1 = 1.2;
            double op2 = 4.7;
            double opRes = op1 + op2;
            int opRes2 = 11 % 2;    // % => modulo (reste de division entière)
            Console.WriteLine($" + {opRes}   %{opRes2}");

            // Opérateur d'incrémentation
            // Pré-incrémentation
            int inc = 0;
            int res = ++inc; // inc=1 res=1
            Console.WriteLine($"inc={inc} res={res}");


            // Post-incrémentation
            inc = 0;
            res = inc++; //res=0 inc=1
            Console.WriteLine($"inc={inc} res={res}");

            // Affectation composée
            double ac = 4.8;
            ac += 15.0;  // équivaut à ac=ac+10.0:

            // Opérateur de comparaison
            ac = 10.0;
            bool comp1 = ac > 30.0; // Une comparaison a pour résultat un booléen
            bool comp2 = ac == 10.0; // true
            Console.WriteLine($"comp1= {comp1} comp2= {comp2}");

            // Opérateur logique
            inc = 1;
            // Opérateur Non
            bool inv = !(inc == 1);     // false
            Console.WriteLine(inv);

            // Opérateur court-circuit && et ||
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool testA = inc < 0.0 && ac == 10.0; // comme inc < 0.0 est faux,  ac == 10.0 n'est pas évalué
                                                  // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool testB = ac == 10.0 || inc < 0.0; // comme ac == 10.0 est vraie,  inc < 0.0 n'est pas évalué
            Console.WriteLine($"testA ={testA} testB={testB}");

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));         // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b110, 2));  //          et => 010
            Console.WriteLine(Convert.ToString(b | 0b110, 2));  //          ou => 11110
            Console.WriteLine(Convert.ToString(b ^ 0b110, 2));  // ou exclusif => 11100
            Console.WriteLine(Convert.ToString(b >> 2, 2));     // Décalage à droite de 2 => 110
            Console.WriteLine(Convert.ToString(b << 1, 2));     // Décalage à gauche de 1 => 110100

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str5 = "azerty";
            string resStr5 = str5 ?? "Default";
            Console.WriteLine(resStr5);         // azerty

            str5 = null;
            resStr5 = str5 ?? "Default";        // Default
            Console.WriteLine(resStr5);
            #endregion

            #region Promotion numérique
            int prni = 11;
            double prnd = 123.4;
            // prni est promu en double => Le type le + petit est promu vers le +grand type des deux
            double prnRes = prni + prnd;
            Console.WriteLine(prnRes);
            Console.WriteLine(prni / 2); // 5
            Console.WriteLine(prni / 2.0); // 5.5   prni est promu en double

            sbyte b1 = 1;
            sbyte b2 = 2;       // sbyte, byte, short, ushort, char sont promus en int
            int b3 = b1 + b2;   // b1 et b2 sont promus en int
            Console.WriteLine(b3);
            #endregion

            #region Exercice
            // Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 __ + __ 3 __ = __ 4
            int va = Convert.ToInt32(Console.ReadLine());
            int vb = Convert.ToInt32(Console.ReadLine());
            int result = va + vb;
            Console.WriteLine($"{va} + {vb}   =  {result}");


            // Moyenne
            // Saisir 2 nombres __entiers__ et afficher la moyenne dans la console
            int m1 = Convert.ToInt32(Console.ReadLine());
            int m2 = Convert.ToInt32(Console.ReadLine());
            double moyenne = (m1 + m2) / 2.0;
            Console.WriteLine($"moyenne={moyenne}");
            #endregion

            #region Enumération
            // motor est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation motor = Motorisation.ELECTRIQUE;

            // enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string strMotor = motor.ToString();
            Console.WriteLine(strMotor);

            // enum ->  entier (cast)
            int iMotor = (int)motor;
            Console.WriteLine(iMotor);

            Direction dir = Direction.NORD;
            short iDir = (short)dir;
            Console.WriteLine(iDir);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            motor = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL");
            Console.WriteLine(motor);

            //dir = (Direction)Enum.Parse(typeof(Direction), "SUD2");    // Si la chaine n'existe pas dans l'énumation => exception
            //Console.WriteLine(dir);

            // entier -> enum
            iDir = 90; //182;
            if (Enum.IsDefined(typeof(Direction), iDir))    // Permet de tester si la valeur entière existe dans l'enumération
            {
                dir = (Direction)iDir;
                Console.WriteLine(dir);
            }
            else
            {
                Console.WriteLine("Valeur qui ne fait parti de l'enumération");
            }

            // Énumération comme indicateurs binaires
            Jour jourSemaine = Jour.LUNDI | Jour.MERCREDI;
            if ((jourSemaine & Jour.LUNDI) != 0)    // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");
            }
            if ((jourSemaine & Jour.MARDI) != 0)    // teste la présence de MARDI
            {
                Console.WriteLine("Mardi");
            }
            jourSemaine = jourSemaine | Jour.SAMEDI;
            if ((jourSemaine & Jour.WEEKEND) != 0)
            {
                Console.WriteLine("WeekEnd");
            }

            switch (dir)
            {
                case Direction.NORD:
                    Console.WriteLine("Nord");
                    break;
                case Direction.SUD:
                    Console.WriteLine("Sud");
                    break;
                default:
                    Console.WriteLine("Autre direction");
                    break;
            }
            #endregion

            #region Structure
            Point pt1;
            pt1.X = 3;  // accès au champs X de la structure
            pt1.Y = -1;
            Console.WriteLine($"P1 (X={pt1.X} , Y={pt1.Y})");

            Point pt2 = pt1;
            Console.WriteLine($"P2 (X={pt2.X} , Y={pt2.Y})");

            pt1.Y = 5;
            Console.WriteLine($"P1 (X={pt1.X} , Y={pt1.Y})");
            Console.WriteLine($"P2 (X={pt2.X} , Y={pt2.Y})");
            if (pt1.X == pt2.X && pt1.Y == pt2.Y)
            {
                Console.WriteLine("Les points sont égaux");
            }

            #endregion
            Console.ReadKey();
        }
    }
}
