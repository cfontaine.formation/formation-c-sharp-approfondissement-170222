﻿using System;

namespace _05_poo
{
    class Adresse
    {
        public string Rue { get; set; }
        public string Ville { get; set; }
        public string CodePostal { get; set; }

        public Adresse(string rue, string ville, string codePostal)
        {
            Rue = rue;
            Ville = ville;
            CodePostal = codePostal;
        }
        public void Afficher()
        {
            Console.WriteLine($" {Rue} ({CodePostal}){Ville}");
        }
    }
}
