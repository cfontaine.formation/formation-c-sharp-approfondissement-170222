﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Poo
{
    internal class Phrase
    {
        List<string> mots;

        public Phrase()
        {
            mots = new List<string>();
        }

        public void  AjoutMot(string mot)
        {
            if (mot != null)
            {
                mots.Add(mot);
            }
        }

       public string this[int pos]
        {
            get
            {
                if (pos < mots.Count && pos>=0)
                {
                    return mots[pos];   
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set {

                if (pos < mots.Count && pos >= 0)
                {
                    mots[pos] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
    }
}
