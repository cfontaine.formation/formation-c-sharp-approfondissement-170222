﻿using System;

namespace _05_poo
{
    // Classe partielle est répartie sur plusieurs fichiers
    partial class Partielle
    {
        public void Methode1()
        {
            Console.WriteLine("Methode1");
            Console.WriteLine(Valeur);
        }
    }
}
