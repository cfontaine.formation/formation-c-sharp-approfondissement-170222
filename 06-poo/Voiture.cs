﻿using System;

namespace _05_poo
{
    /*sealed*/
    class Voiture  //  sealed => pour empecher l'héritage de cette classe
    {
        // Variables d'instances => Etat

        // On n'a plus besoin de déclarer les variables d'instances _marque,_plaqueIma, _compteurKm, elles seront générées automatiquement par les propriétées automatique
        //string marque; 
        //string plaqueIma;
        //int compteurKm = 30;

        string _couleur = "Noir";    // Variable d'instance utilisée par la propriété Couleur (C# 7.0)
        protected int _vitesse;                // Variable d'instance utilisée par la propriété Vitesse (propriété avec une condition)

        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Marque { get; }
        public string PlaqueIma { get; set; }

        public int CompteurKm { get; set; } = 30;   // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)

        // Variable de classe
        // public static int nbVoiture;     // Remplacer par une propriétée static
        public static int NbVoiture { get; private set; }   // On peut associer un modificateur d'accès à get et à set. Il doit être plus restritif que le modificateur de la propriété

        // Agrégation
        public Personne Proprietaire { get; set; }

        // Propriété
        public int Vitesse
        {
            get
            {
                return _vitesse;
            }
            //set
            //{
            //    if (value > 0)
            //    {
            //        vitesse = value;
            //    }
            //}
        }

        // Propriété c# 7.0
        public string Couleur
        {
            get => _couleur;
            set => _couleur = value;
        }

        // Getter / Setter Accesseur (Java/C++) en C# on utilisera les propriétés
        // Getter (accès en lecture)
        //public int GetVitesse()
        //{
        //    return vitesse;
        //}

        // Setter (accès en écriture)
        //public void SetVitesse(int vitesse)
        //{
        //    if(vitesse > 0)
        //    {
        //        this.vitesse = vitesse;
        //    }
        //}


        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            NbVoiture++;// nbVoiture++;
            Console.WriteLine("Constructeur par défaut : Voiture");
        }

        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, string plaqueIma) : this()
        {
            Console.WriteLine("Constructeur 3 paramètres : Voiture");
            Marque = marque;//this.marque = marque;
            this._couleur = couleur;
            PlaqueIma = plaqueIma;// this.plaqueIma = plaqueIma;
        }

        // Chainnage de constructeur : appel du constructeur Voiture(string marque,string couleur, string plaqueIma
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm) : this(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur 5 paramètres : Voiture");
            this._vitesse = vitesse;
            CompteurKm = compteurKm;//this.compteurKm = compteurKm;
        }

        public Voiture(string marque, string couleur, string plaqueIma, Personne proprietaire) : this(marque, couleur, plaqueIma)
        {
            Proprietaire = proprietaire;
        }

        // Constructeur statique: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur static");
        }

        // Destructeur => libérer des ressources
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur");
        //}


        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                //vitesse = vitesse + vAcc;
                _vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                _vitesse -= vFrn;
            }
        }

        public void Arreter()
        {
            _vitesse = 0;
        }

        public bool EstArreter()
        {
            return _vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($"{Marque} {_couleur} {PlaqueIma} {_vitesse} {CompteurKm} ");
            if (Proprietaire != null)
            {
                Console.Write("Propriétaire ");
                Proprietaire.Afficher();
            }
            // Console.Write($"Propriétaire {Proprietaire.Prenom} {Proprietaire.Nom} ");
        }

        // Méthodes de classes
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe"); // On peut accéder dans une méthode de classe à une variable de classe ou à une autre méthode de classe
            Console.WriteLine(NbVoiture);

            //  Console.WriteLine(vitesse);     // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            //  Accelerer(30);                  // ou une méthode d'instance
        }

        public static bool EgalVitesse(Voiture a, Voiture b)
        {
            return a._vitesse == b._vitesse;     // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }
    }
}
