﻿using System;

namespace _05_poo
{
    class Personne
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public Adresse AdressePersonnel { get; set; }

        public Personne(string prenom, string nom, Adresse adressePersonnel)
        {
            Prenom = prenom;
            Nom = nom;
            AdressePersonnel = adressePersonnel;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Prenom} {Nom}");
            AdressePersonnel.Afficher();
        }
    }
}
