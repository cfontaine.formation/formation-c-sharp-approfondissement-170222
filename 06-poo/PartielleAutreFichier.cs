﻿using System;

namespace _05_poo
{
    // Classe partielle est répartie sur plusieurs fichiers
    partial class Partielle
    {
        public int Valeur { get; set; }

        public Partielle(int valeur)
        {
            Valeur = valeur;
        }

        public void Afficher()
        {
            Console.WriteLine(Valeur);
        }
    }
}
