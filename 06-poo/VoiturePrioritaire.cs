﻿using System;

namespace _05_poo
{
    class VoiturePrioritaire : Voiture  // VoiturePrioritaire hérite de Voiture
    {
        public bool Gyro { get; set; }

        public VoiturePrioritaire() //: base()=> appel implicite du constructeur de la classe
        {
            Console.WriteLine("Constructeur par défaut: VoiturePrioritaire");
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma, bool gyro) : base(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur 3 paramètres: VoiturePrioritaire");
            Gyro = gyro;
        }

        public void AlumerGyro()
        {
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        public override void Afficher()
        {
            base.Afficher();
            string tmp = Gyro ? "Gyro allumé" : "Gyro éteint";
            Console.WriteLine(tmp);
        }

        // L'occultation => redéfinir une méthode d'une classe mère et à « casser » le lien vers la classe mère
        public new void Accelerer(int vAcc)
        {
            // base => pour appeler une méthode de la classe mère
            base.Accelerer(2 * vAcc);
        }
    }
}
