﻿using System;

namespace _05_poo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Voiture
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"Nombre voiture= {Voiture.NbVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();
            Console.WriteLine($"Nombre voiture= {Voiture.NbVoiture}");

            // Accès à une propriété en ecriture (set)
            v1.Couleur = "Rouge";

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Marque);
            Console.WriteLine(v1.Vitesse);

            // Appel d’une méthode d’instance
            v1.Accelerer(20);
            Console.WriteLine(v1.Vitesse);
            v1.Freiner(5);
            Console.WriteLine(v1.Vitesse);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());
            v1.Afficher();

            Voiture v2 = new Voiture();
            Console.WriteLine($"Nombre voiture= {Voiture.NbVoiture}");
            Console.WriteLine(v2.Vitesse);
            Console.WriteLine($"Egalité vitesse= {Voiture.EgalVitesse(v1, v2)}");

            Voiture v3 = new Voiture();
            Console.WriteLine($"Nombre voiture= {Voiture.NbVoiture}");
            v3.Afficher();


            Voiture v4 = new Voiture("Honda", "Blanc", "fr-12345-AB");
            Console.WriteLine($"Nombre voiture= {Voiture.NbVoiture}"); //Voiture.nbVoiture
            v4.Afficher();
            Console.WriteLine($"Egalité vitesse= {Voiture.EgalVitesse(v3, v4)}");

            // Test de l'appel du destructeur
            v3 = null;      // En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                            // Il sera détruit lors de la prochaine execution du garbage collector
            GC.Collect();   // Forcer l'appel du garbage collector

            // Initialiseur d'objet
            // Avec un initialiseur d'objet on a accès uniquement à ce qui est public
            Voiture v5 = new Voiture { /*Marque = "Ford",*/ Couleur = "Bleu", PlaqueIma = "fr62-1234NP" };
            Console.WriteLine($"Nombre voiture= {Voiture.NbVoiture}");
            v5.Afficher();
            #endregion

            #region Indexeur

            #endregion

            #region Classe imbriquée
            Conteneur c = new Conteneur();
            c.Test();
            #endregion

            #region  Classe partielle
            Partielle par = new Partielle(42);
            par.Methode1();
            par.Afficher();
            #endregion

            #region Classe Statique
            double val1 = Math.Min(3.5, 1.2);
            Console.WriteLine(Math.PI);
            // Math ma = new Math();    // Math est une classe static, on peut pas l'instancier
            // Elle ne contient que des membres de classe

            // Méthode d'extension
            string strEx = "Azerty";
            Console.WriteLine(strEx.Reverse());
            #endregion

            #region  Agrégation
            Adresse adr1 = new Adresse("46, rue des canonniers", "Lille", "59000");
            Personne per1 = new Personne("John", "Doe", adr1);
            v1.Proprietaire = per1;

            Personne per2 = new Personne("Jane", "Doe", new Adresse("32 Boulevard Vincent Gache", "Nantes", "44200"));
            Voiture v7 = new Voiture("Fiat", "Jaune", "fr59-1234AN", per2);
            v1.Afficher();
            v7.Afficher();
            #endregion

            #region Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire();
            vp1.Accelerer(20);
            vp1.AlumerGyro();
            Console.WriteLine(vp1.Gyro);
            vp1.Afficher();
            VoiturePrioritaire vp2 = new VoiturePrioritaire("Subaru", "Bleu", "fr-1111PO", true);
            vp2.Afficher();
            Voiture vPo = new VoiturePrioritaire();
            vPo.Afficher();
            vPo.Accelerer(100);
            vPo.Afficher();
            #endregion

            Console.ReadKey();
        }
    }
}
