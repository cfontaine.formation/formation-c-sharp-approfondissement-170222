﻿using System;

namespace _05_poo
{
    class Conteneur
    {
        private static string vClasse = "Variable de classe";

        private string vInstance = "Variable d'instance";

        public string Prop { get; set; } = "propriété";


        public void Test()
        {
            Element elm = new Element();
            elm.TestPropriete(this);
            elm.TestVaraibleDeClasse();
            elm.TestVaraibleInstance(this);
        }

        // Classe Imbriqué
        private class Element
        {
            public void TestVaraibleDeClasse()
            {
                Console.WriteLine(vClasse); // on a accès aux variables de classe de la classe conteneur
            }
            public void TestVaraibleInstance(Conteneur c)
            {
                Console.WriteLine(c.vInstance);
            }

            public void TestPropriete(Conteneur c)
            {
                Console.WriteLine(c.Prop);
            }
        }


    }
}
