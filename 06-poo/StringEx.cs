﻿namespace _05_poo
{
    // Méthodes d'extension pour ajouter des fonctionnalités à des classes ou des structures existantes
    // Elle doit être définie dans une classe static
    static class StringEx
    {
        // Une méthode d'extension doit être static et le premier paramètre doit être : this typeEtendu nomParametre
        // Ici, on ajoute une méthode Reverse à la classe .Net string

        public static string Reverse(this string str)
        {
            string tmp = "";
            for (int i = str.Length - 1; i >= 0; i--)
            {
                tmp = tmp + str[i];
            }
            return tmp;
        }
    }
}
