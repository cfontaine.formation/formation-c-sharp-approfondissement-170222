﻿using System;

namespace _03_tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à une dimension
            // Déclarer un tableau
            double[] tab = new double[3];
            // Valeur d'initialisation par défaut des éléments du tableau
            // entier ->0
            // nombre à virgule flottante -> 0.0
            // char -> '\u0000'
            // bool -> false
            // type  référence -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[0]);
            tab[0] = 4.5;
            Console.WriteLine(tab[0]);

            // Console.WriteLine(tab[9]); // si l'on essaye d'accèder à un élément en dehors du tableau => exception

            // Length=>Nombre d'élément du tableau
            Console.WriteLine(tab.Length);

            // Parcourir complétement un tableau (avec un for)
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            foreach (var elm in tab) //  var -> elm est de type double
            {
                Console.WriteLine(elm); // elm uniquement en lecture
            }

            // Déclaration et initialisation
            char[] tabChr = { 'a', 'z', 'e', 'r' };
            foreach (var c in tabChr)
            {
                Console.WriteLine(c);
            }

            // Saisir la taille du tableau
            int size = Convert.ToInt32(Console.ReadLine());
            double[] td = new double[size];
            foreach (var v in td)
            {
                Console.WriteLine(v);
            }
            #endregion

            #region Exercice: Tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7,4,8,0,-3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau


            //   int[] t = { -7, -4, -8, -6, -3 }; //1

            int[] t; // 2
            Console.WriteLine("Entrer la taille du tableau");
            int s = Convert.ToInt32(Console.ReadLine());
            t = new int[s];
            for (int i = 0; i < t.Length; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }
            int max = t[0]; // Int32.MinValue;
            double somme = 0.0;
            foreach (var v in t)
            {
                if (v > max)
                {
                    max = v;
                }
                somme += v; // somme= somme +v
            }
            Console.WriteLine($"maximum={max} moyenne={somme / t.Length}");

            #endregion

            #region Tableau Multidimmension
            int[,] tab2D = new int[4, 3];   // Déclaration d'un tableau à 2 dimensions

            tab2D[0, 2] = 158;              // Accès à un élémént d'un tableau à 2 dimensions
            Console.WriteLine(tab2D[1, 1]);

            // Nombre d'élément du tableau
            Console.WriteLine(tab2D.Length); //12
            // Nombre de dimension du tableau 
            Console.WriteLine(tab2D.Rank); // 2
            // Nombre de ligne du tableau
            Console.WriteLine(tab2D.GetLength(0)); //4
            // Nombre de colonne => 2
            Console.WriteLine(tab2D.GetLength(1)); //3

            // Parcourir complétement un tableau à 2 dimension => for
            for (int i = 0; i < tab2D.GetLength(0); i++)
            {
                for (int j = 0; j < tab2D.GetLength(1); j++)
                {
                    Console.Write($"{tab2D[i, j]}\t");
                }
                Console.WriteLine("");

            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var elm in tab2D)
            {
                Console.WriteLine(elm);

            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            string[,] tabStr = { { "azerty", "rty" }, { "yui", "vbn" }, { "dfg", "ggj" } };
            foreach (var elm in tabStr)
            {
                Console.WriteLine(elm);
            }


            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            char[,,] tab3D = new char[3, 5, 2];
            tab3D[0, 0, 0] = 'A';       // Accès à un élément d'un tableau à 3 dimensions
            Console.WriteLine(tab3D.Rank);      // Nombre de dimension du tableau => 3
            for (int i = 0; i < tab3D.Rank; i++)        // afficher la nombre d'élément de chaque dimmension
            {
                Console.WriteLine(tab3D.GetLength(i));
            }
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau


            // Tableau en escalier
            int[][] tabEsc = new int[3][];
            tabEsc[0] = new int[3];
            tabEsc[1] = new int[2];
            tabEsc[2] = new int[5];

            // Accès à un élément 
            tabEsc[0][1] = 1234;

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length); // 3
            // taille de la première ligne
            Console.WriteLine(tabEsc[0].Length); //3
            // taille de la deuxième ligne
            Console.WriteLine(tabEsc[1].Length); //2

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabEsc.Length; i++)
            {
                for (int j = 0; j < tabEsc[i].Length; j++)
                {
                    Console.Write($"{tabEsc[i][j]}\t");
                }
                Console.WriteLine("");
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (var row in tabEsc) // row type -> int []
            {
                foreach (var elm in row)
                {
                    Console.WriteLine(elm);
                }
            }

            int[][] tabMulti2 = new int[][] { new int[] { 10, 3, 4, 1, 5 }, new int[] { 12, -9 }, new int[] { 1, -1, 12 } };
            foreach (var row in tabMulti2) // row type -> int []
            {
                foreach (var elm in row)
                {
                    Console.WriteLine(elm);
                }
            }

            #endregion
            Console.ReadKey();
        }
    }
}
